/*
    Copyright (c) Rahul Kashyap 2017

    This file is part of PULSEDYN.

    PULSEDYN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    PULSEDYN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PULSEDYN.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef COULOMBPOTENTIAL_H
#define COULOMBPOTENTIAL_H

#include "Particle.h"
#include "boundaryConditions.h"
#include "ChainParticles.h"

class CoulombPotential
{
public:
    CoulombPotential();

    double Getk1() { return k1; }
    /** Set mass
     * \param val New value to set
     */
    void Setk1(double val) { k1 = val; }
    /** Access position
     * \return The current value of position
     */

    double Getk2() { return k2; }
    /** Set mass
     * \param val New value to set
     */
    void Setk2(double val) { k2 = val; }
    /** Access position
     * \return The current value of position
     */

    double Getk3() { return k3; }
    /** Set mass
     * \param val New value to set
     */
    void Setk3(double val) { k3 = val; }
    /** Access position
     * \return The current value of position
     */

    void CalculateAcceleration(std::shared_ptr<ChainParticles>& iChainParticles);
    double TotalPotentialEnergy(std::shared_ptr<ChainParticles> iChainParticles,
        std::vector<double>& oPotentialEnergy);

private:
    // k1 ia the constant in front of the linear term
    double k1 = 0.;

    // k2 is the constant in front of the linear term
    double k2 = 0.;

    // k3 is delta i.e. the bond length
    double k3 = 0.;
};

#endif // POTENTIAL_H