#pragma once
#include <memory>
#include <string>
#include <fstream>

class Logger
{
public:
	static std::shared_ptr<Logger> InitializeOrGetLoggerSingleton();
	void WriteToLog(std::string iLogMessage);
	~Logger();

private:
	static std::shared_ptr<Logger> singleton;
	std::string logFileName = "log.txt";
	std::ofstream logFile;

	Logger();
	void OpenLogFile();
	void CloseLogFile();

};