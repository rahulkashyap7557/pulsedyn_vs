#ifndef CHAINPARTICLES_H
#define CHAINPARTICLES_H

#include <vector>
#include <string>
#include <memory>

class ChainParticles
{
public:
	static std::shared_ptr<ChainParticles> Create();

	void SetSystemSize(unsigned int& iSystemSize);
	void SetLBoundary(std::string& iLBoundary);
	void SetRBoundary(std::string& iRBoundary);
	void SetInitialPerturbations(std::vector<unsigned int>& iPerturbedParticles,
		std::vector<double> iPerturbationAmplitude,
		std::vector<std::string>& iPerturbationType);
	void SetImpurities(std::vector<unsigned int>& iImpurityParticles,
		std::vector<double>& iImpurityValue);
	void SetPosition(std::vector<double>& iPOsition);
	void SetVelocity(std::vector<double>& iVelocity);
	void SetAccel(std::vector<double>& iAccel);
	void CalculateKE();

	void GetPosition(std::vector<double>& oPosition);
	void GetVelocity(std::vector<double>& oVelocity);
	void GetAccel(std::vector<double>& oAccel);
	void GetMass(std::vector<double>& oMass);
	void GetKe(std::vector<double>& oKe);
	void GetTotalKineticEnergy(double& oTotalKineticEnergy);
	void GetSystemSize(unsigned int& oSystemSize);
	void GetLeftBoundary(std::string& oLeftBoundary);
	void GetRightBoundary(std::string& oRightBoundary);


private:
	ChainParticles();
	std::vector<double> mMassVector;
	std::vector<double> mPositionVector;
	std::vector<double> mVelocityVector;
	std::vector<double> mAccelVector;
	std::vector<double> mKeVector;
	std::string mLBoundary;
	std::string mRBoundary;
	unsigned int mSystemSize;

};

#endif // !CHAINPARTICLES_H
