/*
    Copyright (c) Rahul Kashyap 2017

    This file is part of PULSEDYN.

    PULSEDYN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    PULSEDYN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PULSEDYN.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "../include/Output.h"
#include <thread>

std::shared_ptr<Output> Output::singleton = nullptr;

Output::~Output()
{
    this->positionFile.close();
    this->velocityFile.close();
    this->accelerationFile.close();
    this->keFile.close();
    this->peFile.close();
    this->totenFile.close();
    this->restartFile.close();
    this->massFile.close();
}

std::shared_ptr<Output> Output::CreateOrGetSingleton()
{
    if (nullptr == singleton)
    {
        return std::shared_ptr<Output>(new Output());
    }
    else
    {
        return singleton;
    }
    
}

Output::Output()
{
    this->positionFileName = "position.dat";
    this->velocityFileName = "velocity.dat";
    this->accelerationFileName = "acceleration.dat";
    this->keFileName = "ke.dat";
    this->peFileName = "pe.dat";
    this->totenFileName = "totalEnergy.dat";
    this->restartFileName = "restart.dat";
    this->massFileName = "mass.dat";
    OpenFiles();
}



void Output::OpenFiles()
{
    this->positionFile.open(positionFileName.c_str(), std::ofstream::out | std::ofstream::app); // Uncomment this line if you want to append to files instead of overwriting
    this->positionFile.precision(15);

    this->velocityFile.open(velocityFileName.c_str(), std::ofstream::out | std::ofstream::app);
    this->velocityFile.precision(15);

    this->accelerationFile.open(accelerationFileName.c_str(), std::ofstream::out | std::ofstream::app);
    this->accelerationFile.precision(15);

    this->keFile.open(keFileName.c_str(), std::ofstream::out | std::ofstream::app);
    this->keFile.precision(15);

    this->peFile.open(peFileName.c_str(), std::ofstream::out | std::ofstream::app);
    this->peFile.precision(15);

    this->totenFile.open(totenFileName.c_str(), std::ofstream::out | std::ofstream::app);
    this->totenFile.precision(15);

    this->restartFile.open(restartFileName.c_str(), std::ofstream::out | std::ofstream::app);
    this->restartFile.precision(15);

    this->massFile.open(massFileName.c_str(), std::ofstream::out | std::ofstream::app);
    this->massFile.precision(15);
}
//
//void Output::writeToPosFile(double val, bool endline)
//{
//    //std::ofstream positionFile;
//    //positionFile.open (positionFileName.c_str(), std::ofstream::out | std::ofstream::app); // Uncomment this line if you want to append to files instead of overwriting
//    //positionFile.precision(15);
//    this->positionFile << val;
//
//    if (endline)
//    {
//        positionFile << '\n';
//    }
//    else
//    {
//        positionFile << '\t';
//    }
//
//
//}
//
//void Output::writeToVelFile(double val, bool endline)
//{
//    //std::ofstream velocityFile;
//    //velocityFile.open (velocityFileName.c_str(), std::ofstream::out | std::ofstream::app);
//    //velocityFile.precision(15);
//
//    this->velocityFile << val;
//
//    if (endline)
//    {
//        velocityFile << '\n';
//    }
//    else
//    {
//        velocityFile << '\t';
//    }
//
//}
//
//void Output::writeToAccFile(double val, bool endline)
//{
//    //std::ofstream accelerationFile;
//    //accelerationFile.open (accelerationFileName.c_str(), std::ofstream::out | std::ofstream::app);
//    //accelerationFile.precision(15);
//
//    this->accelerationFile << val;
//
//    if (endline)
//    {
//        accelerationFile << '\n';
//    }
//    else
//    {
//        accelerationFile << '\t';
//    }
//
//}
//
//void Output::writeToKeFile(double val, bool endline)
//{
//    //std::ofstream keFile;
//    //keFile.open (keFileName.c_str(), std::ofstream::out | std::ofstream::app);
//    //keFile.precision(15);
//
//    this->keFile << val;
//
//    if (endline)
//    {
//        keFile << '\n';
//    }
//    else
//    {
//        keFile << '\t';
//    }
//
//}

void Output::writeToPeFile(double val, bool endline)
{
    //std::ofstream peFile;
    //peFile.open (peFileName.c_str(), std::ofstream::out | std::ofstream::app);
    //peFile.precision(15);

    this->peFile << val;

    if (endline)
    {
        peFile << '\n';
    }
    else
    {
        peFile << '\t';
    }

}

void Output::writeToTotFile(double val, bool endline)
{
    //std::ofstream totenFile;
    //totenFile.open (totenFileName.c_str(), std::ofstream::out | std::ofstream::app);
    //totenFile.precision(15);

    this->totenFile << val;

    if (endline)
    {
        totenFile << '\n';
    }
    else
    {
        totenFile << '\t';
    }

}
//
void Output::writeToResFile(double& val, bool endline)
{
    //std::ofstream restartFile;
    //restartFile.open (restartFileName.c_str(), std::ofstream::out | std::ofstream::app);
    //restartFile.precision(15);

    this->restartFile << val;

    if (endline)
    {
        restartFile << '\n';
    }
    else
    {
        restartFile << '\t';
    }

}
//
//void Output::writeToMassFile(double val, bool endline)
//{
//    //std::ofstream massFile;
//    //massFile.open (massFileName.c_str(), std::ofstream::out | std::ofstream::app);
//    //massFile.precision(15);
//
//    this->massFile << val;
//
//    if (endline)
//    {
//        massFile << '\n';
//    }
//    else
//    {
//        massFile << '\t';
//    }
//
//}

void Output::writeToVelFile(std::vector<double>& iInputValues)
{
    for (int ii = 0; ii < iInputValues.size(); ii++)
    {
        this->velocityFile << iInputValues[ii];

        int numVals = iInputValues.size();
        
        if (ii < numVals - 1)
        {
            velocityFile << '\t';
        }
        else
        {
            velocityFile << '\n';
        }
    }
}

void Output::writeToPosFile(std::vector<double>& iInputValues)
{
    for (int ii = 0; ii < iInputValues.size(); ii++)
    {
        this->positionFile << iInputValues[ii];

        int numVals = iInputValues.size();

        if (ii < numVals - 1)
        {
            positionFile << '\t';
        }
        else
        {
            positionFile << '\n';
        }
    }
}

void Output::writeToAccFile(std::vector<double>& iInputValues)
{
    for (int ii = 0; ii < iInputValues.size(); ii++)
    {
        this->accelerationFile << iInputValues[ii];

        int numVals = iInputValues.size();

        if (ii < numVals - 1)
        {
            accelerationFile << '\t';
        }
        else
        {
            accelerationFile << '\n';
        }
    }
}

void Output::writeToKeFile(std::vector<double>& iInputValues)
{
    for (int ii = 0; ii < iInputValues.size(); ii++)
    {
        this->keFile << iInputValues[ii];

        int numVals = iInputValues.size();

        if (ii < numVals - 1)
        {
            keFile << '\t';
        }
        else
        {
            keFile << '\n';
        }
    }
}

void Output::writeToPeFile(std::vector<double>& iInputValues)
{
    for (int ii = 0; ii < iInputValues.size(); ii++)
    {
        this->peFile << iInputValues[ii];

        int numVals = iInputValues.size();

        if (ii < numVals - 1)
        {
            peFile << '\t';
        }
        else
        {
            peFile << '\n';
        }
    }
}

void Output::writeToMassFile(std::vector<double>& iInputValues)
{
    for (int ii = 0; ii < iInputValues.size(); ii++)
    {
        this->massFile << iInputValues[ii];

        int numVals = iInputValues.size();

        if (ii < numVals - 1)
        {
            massFile << '\t';
        }
        else
        {
            massFile << '\n';
        }
    }
}

void Output::WriteToFile(std::vector<std::vector<double>>& positionTemp,
    std::vector<std::vector<double>>& velocityTemp,
    std::vector<std::vector<double>>& accelerationTemp,
    std::vector<std::vector<double>>& kineticEnergy,
    std::vector<std::vector<double>>& potentialEnergy,
    std::vector<std::vector<double>>& mass,
    std::vector<double>& totalEnergy)
{
    unsigned int cacheSize = velocityTemp.size();

#pragma omp parallel num_threads(8)
    {
#pragma omp sections
        {
#pragma omp section
            {
                for (int ii = 0; ii < cacheSize; ii++)
                {
                    writeToPosFile(positionTemp[ii]);
                }
                
            }
#pragma omp section
            {
                for (int ii = 0; ii < cacheSize; ii++)
                {
                    writeToVelFile(velocityTemp[ii]);
                }
            }
#pragma omp section
            {
                for (int ii = 0; ii < cacheSize; ii++)
                {
                    writeToAccFile(accelerationTemp[ii]);
                }
                
            }
#pragma omp section
            {
                for (int ii = 0; ii < cacheSize; ii++)
                {
                    writeToKeFile(kineticEnergy[ii]);
                }
            }
#pragma omp section
            {
                for (int ii = 0; ii < cacheSize; ii++)
                {
                    writeToPeFile(potentialEnergy[ii]);
                }
            }
#pragma omp section
            {
                for (int ii = 0; ii < cacheSize; ii++)
                {
                    writeToMassFile(mass[ii]);
                }
            }
#pragma omp section
            {
                for (int ii = 0; ii < cacheSize; ii++)
                {
                    writeToTotFile(totalEnergy[ii], true);
                }
            }
        }
    }

   // writeToTotFile(totalEnergy, false);

    //std::thread posFileThread
    //(
    //    [&]()
    //    {
    //        this->writeToPosFile(positionTemp);
    //    }
    //
    //);

    //std::thread velFileThread
    //(
    //    [&]()
    //    {
    //        this->writeToVelFile(velocityTemp);
    //    }

    //);
    //std::thread accFileThread
    //(
    //    [&]()
    //    {
    //        this->writeToAccFile(accelerationTemp);
    //    }

    //);
    //std::thread keFileThread
    //(
    //    [&]()
    //    {
    //        this->writeToKeFile(kineticEnergy);
    //    }

    //);
    //std::thread massFileThread
    //(
    //    [&]()
    //    {
    //        this->writeToMassFile(mass);
    //    }

    //);
    //std::thread totalEnergyFileThread
    //(
    //    [&]()
    //    {
    //        this->writeToTotFile(totalEnergy, true);
    //    }

    //);
   
    ////outputFileWriter->writeToPosFile(positionTemp);
    ////outputFileWriter->writeToVelFile(velocityTemp);
    ////outputFileWriter->writeToAccFile(accelerationTemp);
    ////outputFileWriter->writeToKeFile(kineticEnergy);
    ////outputFileWriter->writeToMassFile(mass);
    ////outputFileWriter->writeToTotFile(totalEnergy, true);
    //posFileThread.join();    
    //velFileThread.join();
    //accFileThread.join();
    //keFileThread.join();
    //massFileThread.join();
    //totalEnergyFileThread.join();
}
