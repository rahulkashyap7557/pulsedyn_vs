/*
    Copyright (c) Rahul Kashyap 2017

    This file is part of PULSEDYN.

    PULSEDYN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    PULSEDYN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PULSEDYN.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "../include/externalPerturbation.h"

ExternalPerturbation::ExternalPerturbation()
{
    //ctor
    perturbationFunction = "cosine";
    perturbationVariable = "position";
    t1 = 0.;
    t2 = 0.;
    t3 = 0.;
    frequency = 0.;
    ramp = 0.;
    amp = 0.;

}

// RKP 10/26/2019: Change this to make the external perturbation calculation more streamlined.
double ExternalPerturbation::f_forceCalc(double tCurrent)
{
    double perturbation = 0.;
    if (perturbationFunction == "cosine")
    {
        perturbation = f_cosine(tCurrent);
    }
    else if(perturbationFunction == "sine")
    {
        perturbation = f_sine(tCurrent);
    }

    else if(perturbationFunction == "constant")
    {
        perturbation = f_constant(tCurrent);
    }
    return perturbation;
}

double ExternalPerturbation::f_cosine(double tCurrent)
{
    double perturbation;
    double freq1;
    freq1 = frequency + ramp*tCurrent;
    perturbation = amp*cos(freq1*tCurrent);
    return perturbation;
}

double ExternalPerturbation::f_sine(double tCurrent)
{
    double perturbation;
    double freq1;
    freq1 = frequency + ramp*tCurrent;
    perturbation = amp*sin(freq1*tCurrent);
    return perturbation;
}

double ExternalPerturbation::f_constant(double tCurrent)
{
    double perturbation = amp + ramp*tCurrent;
    return perturbation;
}

