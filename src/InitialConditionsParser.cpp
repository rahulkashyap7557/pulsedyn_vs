#include "../include/allIncludes.h"
#include "../include/Logger.h"
#include <iostream>
#include "../include/ChainParticles.h"

InitialConditionsParser::InitialConditionsParser()
{
    BuildOutputFileMapper();
}

void InitialConditionsParser::BuildOutputFileMapper()
{
    outputFileNameMap.insert(std::make_pair(PositionOutputFile, "position.dat"));
    outputFileNameMap.insert((std::make_pair(VelocityOutputFile, "velocity.dat")));
    outputFileNameMap.insert((std::make_pair(AccelerationOutputFile, "acceleration.dat")));
    outputFileNameMap.insert((std::make_pair(KineticEnergyOutputFile, "ke.dat")));
    outputFileNameMap.insert((std::make_pair(PotentialEnergyOutputFile, "pe.dat")));
    outputFileNameMap.insert((std::make_pair(TotalEnergyOutputFile, "totalEnergy.dat")));
    outputFileNameMap.insert((std::make_pair(MassOutputFile, "mass.dat")));
    outputFileNameMap.insert((std::make_pair(RestartOutputFile, "restart.dat")));
}


std::shared_ptr<InitialConditionsParser> InitialConditionsParser::Create()
{
    return std::shared_ptr<InitialConditionsParser>(new InitialConditionsParser);
}

bool InitialConditionsParser::ParseParameterFile()
{
    int fileFlag = 0;
    //int sampleFlag = 0;

    string line, line1;
    unsigned int j = 0;
    unsigned int i;

    std::ifstream parametersFile("parameters.txt");
    //std::ofstream logFile("log.txt");
    std::shared_ptr<Logger> logger = Logger::InitializeOrGetLoggerSingleton();

    //if (!parametersFile)
    if (false == parametersFile.is_open())
    {
        // Turn off commenting below if you want to stop code from running without parameter file.
        //throw std::runtime_error("Can't open parameters file. Running with default values.");
        //logFile << "Can't open parameters file. Running with default values" << endl;
        std::string messageToWrite = "Can't open parameters file. Running with default values";
        logger->WriteToLog(messageToWrite);
        std::cout << messageToWrite << std::endl;
    }
    else
    {
        while (getline(parametersFile, line))
        {
            if (line == "")
            {
                std::cout << "Skipping blank line." << endl;
                std::string messageToWrite = "line " + std::to_string(j + 1) + ": Skipping blank line.";
                logger->WriteToLog(messageToWrite);
                continue;
            }
            else
            {
                std:cout << "Reading from parameter file" << endl;
                std::string messageToWrite = "line " + std::to_string(j + 1) + ": Reading from parameter file.";
                logger->WriteToLog(messageToWrite);
                std::vector<std::string> inputList;

                stringstream ss(line);
                string tmp;
                while (std::getline(ss, tmp, ' '))
                {
                    auto tmp1 = std::regex_replace(tmp, std::regex("\\s+"), "");
                    inputList.push_back(tmp1);
                }
                string a = inputList.at(0);
                string b;
                //             string b = inputList.at(1);
                string c;
                string d;
                string e;
                string f;
                string g;
                string h;
                string ii;
                if (inputList.size() > 1)
                {
                    b = inputList.at(1);
                }

                if (inputList.size() > 2)
                {
                    c = inputList.at(2);
                }
                if (inputList.size() > 3)
                {
                    d = inputList.at(3);
                }
                if (inputList.size() > 4)
                {
                    e = inputList.at(4);
                }
                if (inputList.size() > 5)
                {
                    f = inputList.at(5);
                }
                if (inputList.size() > 6)
                {
                    g = inputList.at(6);
                }
                if (inputList.size() > 7)

                {
                    h = inputList.at(7);
                }
                if (inputList.size() > 8)
                {
                    ii = inputList.at(8);
                }
                int goodCommand = 0; // To check if valid command has been read.

                // Check if comment line is encountered
                if (a == "#")
                {
                    cout << "Encountered comment, skipping to next line." << endl;
                    //logFile << "line " << j + 1 << ":" << "Encountered comment, skipping to next line." << endl;
                    
                    std::string messageToWrite = "line " + std::to_string(j + 1) + ": Encountered comment, skipping to next line.";
                    logger->WriteToLog(messageToWrite);
                    continue;
                }

                // Load model type and parameters
                if (a == "model:")
                {
                    springType = b;
                    if (inputList.size() > 2) {
                        k1 = atof((c.c_str()));
                    }
                    if (inputList.size() > 3) {
                        k2 = atof(d.c_str());
                    }
                    if (inputList.size() > 4) {
                        k3 = atof(e.c_str());
                    }
                    //                if (inputList.size() > 5){
                    //                    k4 = atof(f.c_str());
                    //                }
                    //                if (inputList.size() > 6){
                    //                    k5 = atof(g.c_str());
                    //                }
                    goodCommand = 1;

                }

                // Set up externalPerturbations
                if (a == "force:")
                {
                    if (b == "all")
                    {
                        externalPerturbationParticles.push_back(0);
                    }
                    else
                    {
                        externalPerturbationParticles.push_back(atoi(b.c_str()));
                    }

                    externalPerturbationType.push_back(c);
                    externalPerturbationAmp.push_back(atof(d.c_str()));
                    externalPerturbationT1.push_back(atof(e.c_str()));
                    externalPerturbationT2.push_back(atof(f.c_str()));
                    externalPerturbationT3.push_back(atof(g.c_str()));
                    externalPerturbationRamp.push_back(atof(h.c_str()));
                    externalPerturbationFrequency.push_back(2 * pi / (atof(f.c_str())));
                    externalPerturbationVariable.push_back("force");
                    goodCommand = 1;
                }

                if (a == "externalperturbation:")
                {
                    if (b == "all")
                    {
                        externalPerturbationParticles.push_back(0);
                    }
                    else
                    {
                        externalPerturbationParticles.push_back(atoi(b.c_str()));
                    }
                    externalPerturbationType.push_back(d);
                    externalPerturbationAmp.push_back(atof(e.c_str()));
                    externalPerturbationT1.push_back(atof(f.c_str()));
                    externalPerturbationT2.push_back(atof(g.c_str()));
                    externalPerturbationT3.push_back(atof(h.c_str()));
                    externalPerturbationRamp.push_back(atof(ii.c_str()));
                    externalPerturbationFrequency.push_back(2 * pi / (atof(f.c_str())));
                    externalPerturbationVariable.push_back(c);
                    goodCommand = 1;
                }

                // Set up dissipation
                if (a == "dissipation:")
                {
                    if (b == "all")
                    {
                        dissipationParticles.push_back(0);
                    }
                    else
                    {
                        dissipationParticles.push_back(atoi(b.c_str()));
                    }
                    dissipationStrength.push_back(atof(c.c_str()));
                    goodCommand = 1;
                }

                // Set up boundaries
                if (a == "boundary:")
                {
                    if (b == "left")
                    {
                        std::cout << "this: " << c << endl;
                        lboundary = c;
                    }
                    else if (b == "right")
                    {
                        rboundary = c;
                    }
                    else
                    {
                        std::cout << "Cannot read boundary conditions." << endl;
                        std::cout << "Continuing simulation with default boundaries" << endl;
                    }
                    goodCommand = 1;
                }

                // Set up initial perturbation
                if (a == "init:")
                {
                    // Can open initial conditions from file, if name is given
                    if (b == "file")
                    {
                        fileFlag = 1;
                        std::string fileName = c;
                        std::ifstream initFile(c.c_str());
                        if (!initFile)
                        {
                            // Turn off commenting below if you want to stop code from running without init file.
                            //throw std::runtime_error("Cannot open initial conditions file. Starting with default initial conditions");
                            //logFile << "line " << j + 1 << ":" << "Cannot open initial conditions file. Starting with default initial conditions" << endl;
                            std::string messageToWrite = "line " + std::to_string(j + 1) + ": " + "Cannot open initial conditions file. Starting with default initial conditions";
                            logger->WriteToLog(messageToWrite);
                        
                        }
                        else
                        {
                            int fileLineC = 0;
                            int partic = 0;
                            std::vector<std::string> initList(3);
                            while (getline(initFile, line1))
                            {
                                if (line1 == "")
                                {
                                    std::cout << "Blank line encountered in initial conditions file at line " << fileLineC + 1 << "." << endl;
                                    std::cout << "Skipping blank line in initial conditions file." << endl;
                                    //logFile << "Blank line encountered in initial conditions file at line " << fileLineC + 1 << "." << endl;
                                    //logFile << "Skipping blank line in initial conditions file." << endl;

                                    std::string messageToWrite = "Blank line encountered in initial conditions file at line " + std::to_string(fileLineC + 1) + ".";
                                    messageToWrite += "Skipping blank line in initial conditions file.";
                                    logger->WriteToLog(messageToWrite);
                                }
                                else
                                {
                                    stringstream ss1(line1);
                                    string tmpx, tmpv, tmpa;
                                    ss1 >> tmpx >> tmpv >> tmpa;
                                    partic = fileLineC + 1;

                                    perturbedParticles.push_back(partic);
                                    perturbationType.push_back("pos");
                                    perturbationAmplitude.push_back(atof(tmpx.c_str()));
                                    perturbedParticles.push_back(partic);
                                    perturbationType.push_back("vel");
                                    perturbationAmplitude.push_back(atof(tmpv.c_str()));
                                    perturbedParticles.push_back(partic);
                                    perturbationType.push_back("acc");
                                    perturbationAmplitude.push_back(atof(tmpa.c_str()));
                                    fileLineC += 1;
                                }

                            }
                            systemSize = partic;
                        }
                    }

                    else
                    {
                        perturbedParticles.push_back(atoi(b.c_str()));
                        perturbationType.push_back(c);
                        if (d == "random")
                        {
                            double low = atof(e.c_str());
                            double high = atof(f.c_str());
                            double num = low + (high - low) * (static_cast<double>(rand() % 100)) / 100;
                            perturbationAmplitude.push_back(num);
                        }
                        else
                        {
                            perturbationAmplitude.push_back(atof(d.c_str()));

                        }
                    }
                    goodCommand = 1;
                }

                // Set up masses
                if (a == "mass:")
                {
                    if (b == "all")
                    {
                        impurityParticles.push_back(0);
                    }
                    else
                    {
                        impurityParticles.push_back(atoi(b.c_str()));
                    }
                    impurityValue.push_back(atof(c.c_str()));
                    goodCommand = 1;
                }

                // Set up total run time
                if (a == "recsteps:")
                {
                    totalTime = atof(b.c_str());
                    goodCommand = 1;
                }

                // Set up system size
                if (a == "systemsize:")
                {
                    systemSize = atof(b.c_str());
                    goodCommand = 1;
                }

                // Set up time step
                if (a == "timestep:")
                {
                    dt = atof(b.c_str());
                    goodCommand = 1;
                }

                // Set up sampling steps
                if (a == "printint:")
                {
                    samplingFrequency = atoi(b.c_str());
                    sampleFlag = 1;
                    goodCommand = 1;
                }

                // Set up integration method
                if (a == "method:")
                {
                    method = b;
                    goodCommand = 1;
                }

                if (goodCommand == 0)
                {
                    cout << "Command not recognized. Please make sure the commands are entered correctly in the code." << endl;
                    cout << "If you want to write comments in the parameter file, make sure that you start the line with a #, leave a space and then write your comment." << endl;
                    cout << "Make sure that every comment line is preceded with the comment symbol i.e. #." << endl;
                    
                    std::string messageToWrite = "line " + std::to_string(j + 1) + ":" + "Command not recognized. Please make sure the commands are entered correctly in the code.\n";
                    messageToWrite += "If you want to write comments in the parameter file, make sure that you start the line with a #, leave a space and then write your comment.\n";
                    messageToWrite += "Make sure that every comment line is preceded with the comment symbol i.e. #.\n";
                    logger->WriteToLog(messageToWrite);
                    //logFile << "line " << j + 1 << ":" << "Command not recognized. Please make sure the commands are entered correctly in the code." << endl;
                    //logFile << "If you want to write comments in the parameter file, make sure that you start the line with a #, leave a space and then write your comment." << endl;
                    //logFile << "Make sure that every comment line is preceded with the comment symbol i.e. #." << endl;

                    return 0;
                    //logFile.close();
                }
                cout << line << endl;
                j++;
            }
        }
    }
    //logFile.close();
    parametersFile.close();
    cout << "Read in parameters" << endl;

    if (fileFlag == 1)
    {
        cout << "Read initial conditions from file" << endl;
    }

    return true;
}

void InitialConditionsParser::SetVariables(Simulation& oSimParams, 
    std::shared_ptr<ChainParticles>& oChainparticles,
    //std::vector<Particle>& oChainParticles,
    std::vector<ExternalPerturbation>& oExternalPerturbation)
{
    //oChainParticles = std::vector<Particle>(systemSize);
    oChainparticles->SetSystemSize(systemSize);
    double defaultx = 0.;
    double defaultv = 0.;
    double defaulta = 0.;

    // Next create an object with info about the springs
    if (lboundary == "periodic")
    {
        rboundary = lboundary;
    }
    if (rboundary == "periodic")
    {
        lboundary = rboundary;
    }

    // Initialize positions, velocities and accelerations
    oChainparticles->SetLBoundary(lboundary);
    oChainparticles->SetRBoundary(rboundary);

    //for (int i = 0; i < systemSize; i++)
    //{
    //    oChainParticles[i].Setvelocity(defaultv);
    //    oChainParticles[i].Setposition(defaultx);
    //    oChainParticles[i].Setaccel(defaulta);
    //    oChainParticles[i].Setmass(defaultMass);
    //    oChainParticles[i].Setlboundary(lboundary);
    //    oChainParticles[i].Setrboundary(rboundary);
    //}

    oChainparticles->SetInitialPerturbations(perturbedParticles, perturbationAmplitude, perturbationType);

    //double amp;
    //unsigned int w;

    //if (perturbedParticles.size() > 0)
    //{
    //    for (int i = 0; i < perturbedParticles.size(); i++)
    //    {
    //        w = perturbedParticles.at(i);
    //        amp = perturbationAmplitude.at(i);
    //        if (w - 1 < systemSize)
    //        {
    //            if (perturbationType.at(i) == "vel")
    //            {
    //                oChainParticles[w - 1].Addvelocity(amp);
    //            }
    //            if (perturbationType.at(i) == "pos")
    //            {
    //                oChainParticles[w - 1].Addposition(amp);
    //            }
    //            if (perturbationType.at(i) == "acc")
    //            {
    //                oChainParticles[w - 1].Addaccel(amp);
    //            }
    //        }
    //    }
    //}


    std::string type;
    std::string variable;
    double t1;
    double t2;
    double t3;
    double frequency;
    double ramp;

    // Initialize externalPerturbation object to default values
    //std::vector<ExternalPerturbation> externalPerturbation(systemSize);
    oExternalPerturbation = std::vector<ExternalPerturbation>(systemSize);
    for (int i = 0; i < systemSize; i++)
    {
        oExternalPerturbation[i].SetPerturbationFunction("cosine");
        oExternalPerturbation[i].SetPerturbationVariable("position");
        oExternalPerturbation[i].Sett1(0.);
        oExternalPerturbation[i].Sett2(0.);
        oExternalPerturbation[i].Sett3(0.);
        oExternalPerturbation[i].Setfrequency(0.);
        oExternalPerturbation[i].Setramp(0.);
        oExternalPerturbation[i].Setamp(0.);
        oExternalPerturbation[i].Setgamma(0.);
    }
    // RKP 10/26/2019: Make sure that this data type is changed.
    // Each kind of external perturbation must be stored in an array
    // std::vector<ExternalPerturbation> externalPerturbationList
    // Each of the elements in the vector must contain the particles
    // on which the perturbations act, the time instances, type of variable it acts on,
    //
    // Retrieve initial conditions from values in parameter file
    double amp;
    unsigned int w;
    if (externalPerturbationParticles.size() > 0)
    {
        for (int i = 0; i < externalPerturbationParticles.size(); i++)
        {
            w = externalPerturbationParticles.at(i);
            type = externalPerturbationType.at(i);
            amp = externalPerturbationAmp.at(i);
            t1 = externalPerturbationT1.at(i);
            t2 = externalPerturbationT2.at(i);
            t3 = externalPerturbationT3.at(i);
            frequency = externalPerturbationFrequency.at(i);
            ramp = externalPerturbationRamp.at(i);
            amp = externalPerturbationAmp.at(i);
            variable = externalPerturbationVariable.at(i);
            int j;
            if (w == 0)
            {                
                for (j = 0; j < systemSize; j++)
                {
                    oExternalPerturbation[j].SetPerturbationFunction(type);
                    oExternalPerturbation[j].SetPerturbationVariable(variable);
                    oExternalPerturbation[j].Sett1(t1);
                    oExternalPerturbation[j].Sett2(t2);
                    oExternalPerturbation[j].Sett3(t3);
                    oExternalPerturbation[j].Setfrequency(frequency);
                    oExternalPerturbation[j].Setramp(ramp);
                    oExternalPerturbation[j].Setamp(amp);
                }
            }
            else
            {
                j = w - 1;
                oExternalPerturbation[j].SetPerturbationFunction(type);
                oExternalPerturbation[j].SetPerturbationVariable(variable);
                oExternalPerturbation[j].Sett1(t1);
                oExternalPerturbation[j].Sett2(t2);
                oExternalPerturbation[j].Sett3(t3);
                oExternalPerturbation[j].Setfrequency(frequency);
                oExternalPerturbation[j].Setramp(ramp);
                oExternalPerturbation[j].Setamp(amp);
            }
        }
    }

    // Initialize dissipation object to default values
    for (int i = 0; i < systemSize; i++)
    {
        oExternalPerturbation[i].Setgamma(0.);
    }

    // Retrieve dissipation values from dissipation object
    if (dissipationParticles.size() > 0)
    {
        for (int i = 0; i < dissipationParticles.size(); i++)
        {
            w = dissipationParticles.at(i);
            amp = dissipationStrength.at(i);

            int j; 

            if (w == 0)
            {
                for (j = 0; j < systemSize; j++)
                {
                    oExternalPerturbation[j].Setgamma(amp);
                }
            }
            else
            {
                j = w - 1;
                oExternalPerturbation[j].Setgamma(amp);
            }
        }
    }

    // Set mass is 1.0 first
    //for (int i = 0; i < systemSize; i++)
    //{
    //    mass.push_back(defaultMass);
    //}

    oChainparticles->SetImpurities(impurityParticles, impurityValue);
    // Retrieve values of impurities if there are any
    //if (impurityParticles.size() > 0)
    //{
    //    for (int i = 0; i < impurityParticles.size(); i++)
    //    {
    //        w = impurityParticles.at(i);
    //        amp = impurityValue.at(i);
    //        int j;
    //        if (w == 0)
    //        {
    //            for (j = 0; j < systemSize; j++)
    //            {
    //                oChainParticles[j].Setmass(amp);
    //            }
    //        }
    //        else
    //        {
    //            j = w - 1;
    //            oChainParticles[j].Setmass(amp);
    //        }
    //    }
    //}

    // Check that mass is greater than 0
    //for (int i = 0; i < systemSize; i++)
    //{
    //    amp = oChainParticles.at(i).Getmass();
    //    if (amp <= 0.00000000001)
    //    {
    //        oChainParticles[i].Setmass(defaultMass);
    //        cout << "Mass at %d th particle was less than or equal to 0." << endl;
    //    }
    //}

    // Create simulation object
    
    if (sampleFlag == 0)
    {
        samplingFrequency = 1 / dt;
        if (samplingFrequency < 1)
        {
            samplingFrequency = 10;
        }
    }

    oSimParams.SettimeStep(dt);
    oSimParams.SetsamplingFrequency(samplingFrequency);
    oSimParams.SettotalTime(totalTime);
    oSimParams.Setmethod(method);
    oSimParams.SetsystemSize(systemSize);
    //oSimParams.SetSpringType(springType);
}

std::string InitialConditionsParser::GetSpringType()
{
    return springType;
}


// Check if any existing files with same names that we wish to write to
bool InitialConditionsParser::CheckAndDeleteExistingOutputFiles()
{
    for (std::map<OUTPUT_FILE_NAMES, string>::iterator iter = outputFileNameMap.begin();
         iter != outputFileNameMap.end();
         iter++)
    {
        std::string fileName = iter->second;
        std::string errOut;

        if(ifstream(fileName.c_str()))
        {
            std::cout << "Existing file will be deleted - "<< fileName << std::endl;
            if( remove( fileName.c_str() ) != 0 )
            {
                errOut = "Error deleting file " + fileName;
                perror(  fileName.c_str() );
                std::cout << std::endl;
            }
            else
            {
                errOut = fileName + " successfully deleted.";
                puts( errOut.c_str() );
                std::cout << std::endl;
            }
        }
    }
    // Check if file exists and delete if it does.

    // TODO: for now returning true, but should return if failed to delete files already present
    return true;

}

double InitialConditionsParser::GetK1()
{
    return k1;
}

double InitialConditionsParser::GetK2()
{
    return k2;
}

double InitialConditionsParser::GetK3()
{
    return k3;
}

