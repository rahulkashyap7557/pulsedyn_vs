#include "..\include\Integrator.h"
#include "..\include\Gear5Integrator.h"

std::shared_ptr<Integrator> Integrator::Create(std::string iTypeOfIntegrator)
{
	//if (iTypeOfIntegrator == "gear5")
	//{
		return std::shared_ptr<Integrator>(new Gear5Integrator());
	//}
	
}

double Integrator::GetTimeStep()
{
	return mTimeStep;
}

void Integrator::SetTimeStep(double iTimeStep)
{
	mTimeStep = iTimeStep;
}
